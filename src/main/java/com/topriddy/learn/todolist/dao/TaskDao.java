package com.topriddy.learn.todolist.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.topriddy.learn.todolist.core.Task;
import com.topriddy.learn.todolist.core.User;
import io.dropwizard.hibernate.AbstractDAO;

public class TaskDAO extends AbstractDAO<Task> {
    public TaskDAO(SessionFactory factory) {
        super(factory);
    }

    public long create(Task task) {
        return persist(task).getId();
    }

    public List<Task> findAllByUser(User user) {
        return criteria().add(Restrictions.eq("user", user)).list();
    }
}