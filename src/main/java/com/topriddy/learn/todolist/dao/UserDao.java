package com.topriddy.learn.todolist.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;

import com.topriddy.learn.todolist.core.User;
import io.dropwizard.hibernate.AbstractDAO;

public class UserDAO extends AbstractDAO<User> implements Serializable {
    public UserDAO(SessionFactory factory) {
        super(factory);
    }

    public User findById(Long id) {
        return get(id);
    }

    public List<User> findAll() {
        return criteria().list();
    }

    public long create(User user) {
        return persist(user).getId();
    }
}