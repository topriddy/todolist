package com.topriddy.learn.todolist.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;

/**
 * @author Temitope.Faro
 */
@Path(("/ping"))
public class PingResource {

    @GET
    @Timed
    public Response ping() {
        return Response.ok("pong").build();
    }
}