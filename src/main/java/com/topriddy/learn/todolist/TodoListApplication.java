package com.topriddy.learn.todolist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.topriddy.learn.todolist.core.Task;
import com.topriddy.learn.todolist.dao.TaskDAO;
import com.topriddy.learn.todolist.dao.UserDAO;
import com.topriddy.learn.todolist.resources.PingResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TodoListApplication extends Application<TodoListConfiguration> {
    private static final Logger log = LoggerFactory.getLogger(TodoListApplication.class);

    private final HibernateBundle<TodoListConfiguration> hibernate = new HibernateBundle<TodoListConfiguration>(
            Task.class) {
        @Override
        public DataSourceFactory getDataSourceFactory(TodoListConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public void initialize(Bootstrap<TodoListConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(TodoListConfiguration configuration, Environment environment) throws Exception {
        log.info("Method App#run()");

        //register hibernate daos
        final UserDAO userDAO = new UserDAO(hibernate.getSessionFactory());
        final TaskDAO taskDAO = new TaskDAO(hibernate.getSessionFactory());

        //register rest services
        final PingResource pingResource = new PingResource();
        environment.jersey().register(pingResource);
    }

    public static void main(String args[]) throws Exception {
        new TodoListApplication().run(args);
    }
}