package com.topriddy.learn.todolist.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.topriddy.learn.todolist.core.User;
import com.topriddy.learn.todolist.core.UserMother;

public class UserDAOTest extends AbstractDAOTest {

    private UserDAO userDAO;

    @Before
    public void before() {
        userDAO = new UserDAO(hibernateRule.getSessionFactory());
    }

    @Test
    public void shouldCreateUserSuccessfully() {
        User user = new UserMother().build();
        userDAO.create(user);
        assertThat(user.getId()).isNotNull();
    }

    @Test
    public void shouldFindUserGivenExistingId() {
        User user = new UserMother().build();
        userDAO.create(user);

        User foundUser = userDAO.findById(user.getId());
        assertThat(foundUser.getId()).isNotNull();
    }

    @Test
    public void shouldFindAllUsers() {
        User user1 = new UserMother().username("foo").build();
        userDAO.create(user1);

        User user2 = new UserMother().username("bar").build();
        userDAO.create(user2);

        List<User> users = userDAO.findAll();
        assertThat(users).hasSize(2);
    }
}