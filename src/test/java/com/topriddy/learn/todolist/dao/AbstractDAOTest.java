package com.topriddy.learn.todolist.dao;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AbstractDAOTest {
    private static final Logger LOG = LoggerFactory.getLogger(AbstractDAOTest.class);

    @Rule
    public final HibernateRule hibernateRule = new HibernateRule();

    @Before
    public void setup() {
        hibernateRule.beginTransaction();
    }

    @After
    public void after() {
        try {
            hibernateRule.commit();
        } catch (HibernateException e) {
            LOG.error("Failed to commit, trying rollback instead", e);
            hibernateRule.rollback();
        }
    }
}
