package com.topriddy.learn.todolist.dao;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.ImprovedNamingStrategy;
import org.hibernate.service.ServiceRegistry;
import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

import com.topriddy.learn.todolist.core.Task;
import com.topriddy.learn.todolist.core.User;
import lombok.Getter;

public class HibernateRule implements MethodRule {
    @Getter
    private SessionFactory sessionFactory;
    private final Class<?>[] entities = { User.class, Task.class };

    @Override
    public Statement apply(Statement statement, FrameworkMethod frameworkMethod, Object o) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                createSessionFactory();
                try {
                    statement.evaluate();
                    closeSession();
                } finally {

                }
            }
        };
    }

    public void createSessionFactory() {
        Configuration config = new Configuration();
        config.setProperty("hibernate.connection.url", "jdbc:postgresql://127.0.0.1:5432/todolist");
        config.setProperty("hibernate.connection.username", "topriddy");
        config.setProperty("hibernate.connection.password", "password");
        config.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");
        config.setProperty("hibernate.current_session_context_class", "thread");
        config.setProperty("hibernate.show_sql", "true");
        config.setNamingStrategy(ImprovedNamingStrategy.INSTANCE);
        config.setProperty("hibernate.hbm2ddl.auto", "create-drop");

        for (int i = 0; i < entities.length; i++) {
            config.addAnnotatedClass(entities[i]);
        }
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
                                                                              .build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
    }

    public void createSessionFactoryH2DB() {
        Configuration config = new Configuration();
        config.setProperty("hibernate.connection.url", "jdbc:h2:mem:test");
        config.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        config.setProperty("hibernate.current_session_context_class", "thread");
        config.setProperty("hibernate.show_sql", "true");
        config.setNamingStrategy(ImprovedNamingStrategy.INSTANCE);
        config.setProperty("hibernate.hbm2ddl.auto", "create-drop");
        config.setProperty("hibernate.dialect",
                "org.hibernate.dialect.H2Dialect");
        for (int i = 0; i < entities.length; i++) {
            config.addAnnotatedClass(entities[i]);
        }
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
                                                                              .build();
        sessionFactory = config.buildSessionFactory(serviceRegistry);
    }

    public Session getSession() {
        try {
            return sessionFactory.getCurrentSession();
        } catch (SessionException se) {
            return sessionFactory.openSession();
        }
    }

    public void beginTransaction() {
        getSession().getTransaction().begin();
    }

    public void commit() {
        getSession().getTransaction().commit();
    }

    public void rollback() {
        getSession().getTransaction().rollback();
    }

    public void closeSession() {
        getSession().close();
    }

    public void closeTransaction() {
        getSession().getTransaction().commit();
    }
}
