package com.topriddy.learn.todolist.dao;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.topriddy.learn.todolist.core.Task;
import com.topriddy.learn.todolist.core.TaskMother;
import com.topriddy.learn.todolist.core.User;
import com.topriddy.learn.todolist.core.UserMother;

public class TaskDAOTest extends AbstractDAOTest {
    private UserDAO userDAO;
    private TaskDAO taskDAO;

    @Before
    public void before() {
        taskDAO = new TaskDAO(hibernateRule.getSessionFactory());
        userDAO = new UserDAO(hibernateRule.getSessionFactory());
    }

    @Test
    public void shouldCreateTask() {
        User user = new UserMother().build();
        userDAO.create(user);

        Task task = new TaskMother().user(user).build();
        taskDAO.create(task);

        assertThat(task.getId()).isNotNull();
    }
}