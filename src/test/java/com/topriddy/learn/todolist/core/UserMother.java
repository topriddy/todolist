package com.topriddy.learn.todolist.core;

public class UserMother {

    private String username = "topriddy";

    public UserMother username(String username) {
        this.username = username;
        return this;
    }

    public User build() {
        User user = new User();
        user.setUsername(username);
        return user;
    }
}