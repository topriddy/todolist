package com.topriddy.learn.todolist.core;

import java.util.UUID;

import org.joda.time.DateTime;

public class TaskMother {
    private String name = "someTask";
    private DateTime whenDateTime = DateTime.now();
    private User user = null;
    private UUID taskReference = UUID.randomUUID();

    public TaskMother() {
        user = new UserMother().build();
    }

    public TaskMother name(String name) {
        this.name = name;
        return this;
    }

    public TaskMother whenDateTime(DateTime whenDateTime) {
        this.whenDateTime = whenDateTime;
        return this;
    }

    public TaskMother user(User user) {
        this.user = user;
        return this;
    }

    public TaskMother taskReference(UUID taskReference) {
        this.taskReference = taskReference;
        return this;
    }

    public Task build() {
        Task task = new Task();
        task.setName(name);
        task.setWhenDateTime(whenDateTime);
        task.setUser(user);
        task.setTaskReference(taskReference);
        return task;
    }
}
