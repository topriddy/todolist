package com.topriddy.learn.todolist.resources;

import static org.assertj.core.api.Assertions.assertThat;

import javax.ws.rs.core.Response;

import org.junit.Test;

public class PingResourceTest {
    private final PingResource pingResource = new PingResource();

    @Test
    public void testPing() throws Exception {
        Response response = pingResource.ping();

        String result = (String) response.getEntity();
        assertThat(result).isEqualTo("pong");
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());
    }
}